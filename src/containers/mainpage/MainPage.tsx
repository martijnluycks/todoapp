import React from 'react';
import './MainPage.scss';
import NavBar from './navbar/NavBar';
import ToDoContainer from '../todo/ToDoContainer';
import { Application, useGlobalUIStateManagerService } from '../../services/GlobalUIStateManagerService';
// @ts-ignore
import SuggestionsForm from '../suggestions/SuggestionsForm';
import Other from '../other/Other';

const SelectApplication = () => {
  // A way to avoid routing...
  const globalUiStateManagerService = useGlobalUIStateManagerService();
  if (globalUiStateManagerService.application === Application.ToDos) {
    return <ToDoContainer />;
  } if (globalUiStateManagerService.application === Application.Suggestions) {
    return <SuggestionsForm />;
  } if (globalUiStateManagerService.application === Application.Other) {
    return <Other />;
  }
  return null;
};

const MainPage = (): JSX.Element => {
  const globalUiStateManagerService = useGlobalUIStateManagerService();
  return (
    <div className="gridwrapper">
      <div className="box fullrow">
        { `THE-todo-app! Busy: ${globalUiStateManagerService.isBusy}, Application: ${globalUiStateManagerService.application}` }
      </div>
      <NavBar />
      <div className="box sideleft">SideLeft</div>
      <div className="box maincontent">
        <SelectApplication />
      </div>
      <div className="box sideright">SideRight</div>
      <div className="box fullrow">Footer</div>
    </div>
  );
};

export default MainPage;
