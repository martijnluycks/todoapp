import React from 'react';
import Button from '../../../components/Button';
import { Application, useGlobalUIStateManagerService } from '../../../services/GlobalUIStateManagerService';

const NavBar = (): JSX.Element => {
  const { application, setApplication } = useGlobalUIStateManagerService();

  const handleClick = (a: Application) => {
    setApplication(a);
  };

  return (
    <div className="box fullrow">
      {
          Object.keys(Application).map((k) => {
            if (k in Application) {
              return (
                <Button
                  key={k}
                  value={k}
                  clickHandler={() => handleClick(k as Application)}
                  disabledExpression={application === k}
                />
              );
            }
            return null;
          })
      }
    </div>
  );
};

export default NavBar;
