/* eslint-disable react/jsx-props-no-spreading,max-len */
import React from 'react';
import { Form, Field } from 'react-final-form';
import Button from '../../components/Button';
import Input from '../../components/Input';

const SuggestionsForm = () => {
  const suggestionsFormData = {
    firstName: '',
    lastName: '',
    notes: '',
  };

  const onSubmit = async (formValues, form) => {
    sessionStorage.setItem('suggestionsForm', JSON.stringify(formValues));
    await Object.keys(formValues).forEach((key) => {
      form.change(key, undefined);
      form.resetFieldState(key);
    });
    form.reset(suggestionsFormData);
  };

  const validateRequired = (value) => (value ? undefined : 'Required');
  const validate1234NotAllowed = (value) => (value === '1234' ? '1234 is not allowed!' : undefined);

  // Master this composeValidators function.. very hard to 'get' what happens here at 1st glance.
  // eslint-disable-next-line max-len
  const composeValidators = (...validators) => (value) => validators.reduce((error, validator) => error || validator(value), undefined);

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={
        suggestionsFormData
      }
      render={({
        handleSubmit, form, submitting, pristine, values, valid,
      }) => (
        <form onSubmit={handleSubmit}>
          <Field name="firstName" validate={validateRequired}>
            {({ input, meta }) => (
              <div>
                <Input {...input} type="text" placeholder="First Name" id="firstName" />
                {(meta.error || meta.submitError) && meta.touched && (
                  <span>{meta.error || meta.submitError}</span>
                )}
              </div>
            )}
          </Field>

          <Field name="lastName" validate={validateRequired}>
            {({ input, meta }) => (
              <div>
                <Input {...input} type="text" placeholder="Last Name" id="lastName" />
                {(meta.error || meta.submitError) && meta.touched && (
                  <span>{meta.error || meta.submitError}</span>
                )}
              </div>
            )}
          </Field>

          <Field name="notes" placeholder="Notes" validate={composeValidators(validateRequired, validate1234NotAllowed)}>
            {({ input, meta }) => (
              <div>
                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                <Input {...input} type="text" placeholder="Notes (1234 is not allowed!)" id="notes" size={400} />
                {meta.error && meta.touched && <span>{meta.error}</span>}
              </div>
            )}
          </Field>

          <Button
            value="Submit"
            type="submit"
            disabledExpression={submitting || pristine || !valid}
          />
          <Button
            value="Reset"
            type="button"
            disabledExpression={submitting || pristine || (Object.keys(values).forEach((value) => value === undefined))}
            clickHandler={form.reset}
          />
          <pre>{JSON.stringify(values, 0, 2)}</pre>
        </form>
      )}
    />
  );
};

export default SuggestionsForm;
