import React, { useEffect } from 'react';
import ToDoList from './components/ToDoList';
import ToDoHeader from './components/ToDoHeader';
import { useToDoManagerService } from '../../services/ToDoManagerService';

const ToDoContainer = (): JSX.Element => {
  // Specific data state management
  const toDoManagerService = useToDoManagerService();

  useEffect(() => {
    refreshToDos();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const refreshToDos = async () => {
    await toDoManagerService.refreshToDos();
  };
  return (
    <>
      <ToDoHeader name="Todo's:" />
      <ToDoList toDos={toDoManagerService.todos} />
    </>
  );
};

export default ToDoContainer;
