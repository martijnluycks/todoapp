/* eslint-disable max-len */
import React, { useState, BaseSyntheticEvent } from 'react';
import ToDoListItemActions from './ToDoListItemActions';
import ToDoListItemContents from './ToDoListItemContents';
import ToDo from '../../../interfaces/ToDo.interface';
import { useToDoManagerService } from '../../../services/ToDoManagerService';
import './ToDoListItem.scoped.scss';

interface ToDoListItemProps {
    todo: ToDo,
    isEditMode: boolean,
}

const ToDoListItem = ({
  todo, isEditMode,
}: ToDoListItemProps): JSX.Element => {
  const [toDoItem, setToDoItem] = useState(todo);
  const [isThisItemChanged, setIsThisItemChanged] = useState(false);

  const toDoManagerService = useToDoManagerService();

  const handleChange = (e: BaseSyntheticEvent) => {
    setIsThisItemChanged(true);
    if (e.target.type === 'checkbox') {
      setToDoItem({
        ...toDoItem,
        [e.target.name]: e.target.checked,
      });
    } else {
      setToDoItem({
        ...toDoItem,
        [e.target.name]: e.target.value,
      });
    }
  };

  const handleCancelAdd = async () => {
    const originalToDo = await toDoManagerService.emptyToDo;
    setToDoItem(originalToDo);
    setIsThisItemChanged(false);
  };

  const handleCancelUpdate = async () => {
    if (toDoItem.id !== undefined) {
      const originalToDo = await toDoManagerService.getToDoFromServer(toDoItem.id);
      setToDoItem(originalToDo);
    }
    setIsThisItemChanged(false);
  };

  const handleToggleComplete = async () => {
    const prevCompleteState = toDoItem.complete;
    const newVersionOfToDo = {
      ...toDoItem,
      complete: !prevCompleteState,
    };
    await toDoManagerService.updateToDoOnServer(newVersionOfToDo);
    setToDoItem(newVersionOfToDo);
    setIsThisItemChanged(false);
  };

  return (
    <div className="todoitemcontainer">
      <ToDoListItemContents
        className="todolistitemcontents"
        toDoItem={toDoItem}
        handleChange={handleChange}
        isEditMode={isEditMode}
        handleToggleComplete={handleToggleComplete}
      />
      <ToDoListItemActions
        className="todolistitemactions"
        toDoItem={toDoItem}
        isChanged={isThisItemChanged}
        setIsChanged={setIsThisItemChanged}
        handleCancelAdd={handleCancelAdd}
        handleCancelUpdate={handleCancelUpdate}
        isEditMode={isEditMode}
      />
      <br />
    </div>
  );
};

export default ToDoListItem;
