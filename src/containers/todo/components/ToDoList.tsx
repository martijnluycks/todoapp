import React, { useState } from 'react';
import ToDoListItem from './ToDoListItem';
// import { useToDoManagerService } from '../../../services/ToDoManagerService';
import ToDo from '../../../interfaces/ToDo.interface';
import Button from '../../../components/Button';

const ToDoList = ({ toDos }:{toDos: ToDo[]}): JSX.Element => {
  // Specific UI State part
  const [isEditMode, setIsEditMode] = useState(false);

  const toggleEditMode = () => {
    setIsEditMode(() => !isEditMode);
  };

  return (
    <div>
      { toDos.map((todo: ToDo) => {
        if (todo.id !== undefined) {
          return (
            <ToDoListItem
              key={todo.id}
              isEditMode={isEditMode}
              todo={todo}
            />
          );
        }
        if (isEditMode) {
          return (
            <ToDoListItem
              key={todo.title}
              isEditMode={isEditMode}
              todo={todo}
            />
          );
        }
        return (
          <div key="nothing" />
        );
      })}
      <Button
        key="toggleEdit"
        value={isEditMode ? 'Overview' : 'Edit'}
        clickHandler={toggleEditMode}
        disabledExpression={false}
      />
    </div>
  );
};

export default ToDoList;
