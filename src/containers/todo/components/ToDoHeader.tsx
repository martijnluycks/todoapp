import React from 'react';

interface ToDoHeaderProps {
    name: string,
}

const ToDoHeader = ({ name }: ToDoHeaderProps): JSX.Element => (
  <div>{name}</div>
);

export default ToDoHeader;
