import React from 'react';
import Button from '../../../components/Button';
import ToDo from '../../../interfaces/ToDo.interface';
import { useToDoManagerService } from '../../../services/ToDoManagerService';

interface ToDoListItemActionsProps {
  toDoItem: ToDo,
  isChanged: boolean,
  setIsChanged: (isChanged: boolean) => void,
  handleCancelAdd: () => void,
  handleCancelUpdate: () => void,
  isEditMode: boolean,
  className: any,
}

const ToDoListItemActions = ({
  // eslint-disable-next-line max-len
  toDoItem, isChanged, setIsChanged, handleCancelAdd, handleCancelUpdate, isEditMode, className,
}: ToDoListItemActionsProps): JSX.Element => {
  const toDoManagerService = useToDoManagerService();
  const handleClickSave = () => {
    if (!toDoItem.id) {
      handleAdd(toDoItem);
    } else {
      handleUpdate(toDoItem);
    }
    setIsChanged(false);
  };

  const handleAdd = async (toDo: ToDo) => {
    await toDoManagerService.addToDoToServer(toDo);
    handleCancelAdd();
  };

  const handleUpdate = async (toDo: ToDo) => {
    await toDoManagerService.updateToDoOnServer(toDo);
  };

  const handleClickDelete = () => {
    if (toDoItem.id === undefined) {
      handleCancelAdd();
    } else {
      handleDelete();
    }
  };

  const handleDelete = async () => {
    await toDoManagerService.deleteToDoFromServer(toDoItem);
  };

  const handleClickCancel = () => {
    if (toDoItem.id === undefined) {
      handleCancelAdd();
    } else {
      handleCancelUpdate();
    }
  };

  if (!isEditMode) {
    return (
      <>
      </>
    );
  }
  return (
    <div className={className}>
      <Button
        value="Save"
        clickHandler={handleClickSave}
        disabledExpression={!isChanged}
      />
      <Button
        value="Delete"
        clickHandler={handleClickDelete}
        disabledExpression={!toDoItem.id || isChanged}
      />
      <Button
        value="Cancel"
        clickHandler={handleClickCancel}
        disabledExpression={!isChanged}
      />
    </div>
  );
};

export default ToDoListItemActions;
