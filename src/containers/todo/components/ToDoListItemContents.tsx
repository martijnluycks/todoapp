import React, { BaseSyntheticEvent } from 'react';
import ToDo from '../../../interfaces/ToDo.interface';
import Input from '../../../components/Input';
import CheckBox from '../../../components/CheckBox';

interface ToDoListItemContentsProps {
    toDoItem: ToDo,
    handleChange: (event: BaseSyntheticEvent) => void,
    isEditMode: boolean,
    handleToggleComplete: () => void;
    className: any;
}

// eslint-disable-next-line max-len
const ToDoListItemContents = ({
  toDoItem, handleChange, isEditMode, handleToggleComplete, className,
}: ToDoListItemContentsProps): JSX.Element => (
  <div className={className}>
    {/*    <Input
      name="id"
      placeholder="id"
      value={`${toDoItem.id}`}
      handleChange={handleChange}
      disabled
      size={75}
    /> */}
    <Input
      name="title"
      placeholder="Title"
      value={toDoItem.title}
      handleChange={handleChange}
      disabled={!isEditMode}
      size={150}
    />
    <Input
      name="description"
      placeholder="Description"
      value={toDoItem.description}
      disabled={!isEditMode}
      handleChange={handleChange}
      size={225}
    />
    <CheckBox
      name="complete"
      checked={toDoItem.complete}
      handleChange={handleToggleComplete}
      disabled={isEditMode}
    />
  </div>
);

export default ToDoListItemContents;
