/* eslint-disable react/jsx-props-no-spreading */
import React, { BaseSyntheticEvent } from 'react';
import './Input.scoped.scss';

interface InputProps {
  name: string,
  placeholder: string,
  value: string,
  handleChange: (event: BaseSyntheticEvent) => void,
  disabled: boolean,
  size: number,
}
const inlineStyle = (size: number) => ({
  width: size,
});

const Input:React.FC<InputProps> = ({
  name, placeholder, value, handleChange, disabled, size, ...props
}): JSX.Element => (
  <input
    type="input"
    name={name}
    placeholder={placeholder}
    value={value}
    onChange={handleChange}
    disabled={disabled}
    size={size}
    style={inlineStyle(size)}
    className="input"
    {...props}
  />
);
export default Input;
