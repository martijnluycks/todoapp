import React, { BaseSyntheticEvent } from 'react';
import './Button.scoped.scss';

interface ButtonProps {
  // eslint-disable-next-line react/require-default-props
  type?: string
  value: string,
  // eslint-disable-next-line react/require-default-props
  clickHandler?: (event: BaseSyntheticEvent) => void,
  disabledExpression: boolean
}

const Button = ({
  type, value, clickHandler, disabledExpression,
}: ButtonProps): JSX.Element => (
  <input
    type={type || 'button'}
    value={value}
    onClick={clickHandler}
    disabled={disabledExpression}
    className={disabledExpression ? 'button-disabled' : 'button-enabled'}
  />
);

export default Button;
