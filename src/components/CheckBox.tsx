import React, { BaseSyntheticEvent } from 'react';

interface CheckBoxProps {
  name: string,
  checked: boolean,
  handleChange: (event: BaseSyntheticEvent) => void,
  disabled: boolean
}

const CheckBox = ({
  // eslint-disable-next-line react/prop-types
  name, checked, handleChange, disabled,
}: CheckBoxProps): JSX.Element => (
  <input
    type="checkbox"
    name={name}
    checked={checked}
    onChange={handleChange}
    disabled={disabled}
  />
);
export default CheckBox;
