import React from 'react';
import { useGlobalUIStateManagerService } from '../services/GlobalUIStateManagerService';
import './SpinnerFullPageModal.scoped.scss';

const SpinnerFullPageModal = (): JSX.Element | null => {
  const ui = useGlobalUIStateManagerService();
  if (ui.isBusy) {
    return (
      <div className="overlay">
        <div className="spinner" />
      </div>
    );
  }
  return null;
};
export default SpinnerFullPageModal;
