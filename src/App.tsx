import React from 'react';
import './App.scss';
import SpinnerFullPageModal from './components/SpinnerFullPageModal';

import MainPage from './containers/mainpage/MainPage';
import {
  GlobalUIStateManagerServiceProvider,
  useGlobalUIStateManagerService,
} from './services/GlobalUIStateManagerService';
import { ToDoManagerServiceProvider } from './services/ToDoManagerService';

const App = (): JSX.Element => (
  <GlobalUIStateManagerServiceProvider>
    <GlobalUIStateManagerServiceProviderContext />
  </GlobalUIStateManagerServiceProvider>
);

const GlobalUIStateManagerServiceProviderContext = () => {
  const uiService = useGlobalUIStateManagerService();
  return (
    <ToDoManagerServiceProvider ui={uiService}>
      <SpinnerFullPageModal />
      <MainPage />
    </ToDoManagerServiceProvider>
  );
};
export default App;
