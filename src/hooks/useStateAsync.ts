import { Dispatch, SetStateAction, useState } from 'react';

const useStateAsync = <S>(initialState: S): [S, Dispatch<SetStateAction<S>>, () => Promise<S>] => {
  const [state, setState] = useState(initialState);

  const getState = async () : Promise<S> => {
    // eslint-disable-next-line no-shadow
    let state : S;

    // By calling setState with a function, the provided function is guaranteed
    // to run right AFTER the state update.
    // So the currentState is the actual state, which is assigned to the shadow
    // state variable, which is returned from the getState.
    await setState((currentState: S) => {
      state = currentState;
      return currentState;
    });

    // @ts-ignore
    return state;
  };
  return [state, setState, getState];
};
export default useStateAsync;
