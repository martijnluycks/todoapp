/* eslint-disable semi */
import ToDo from '../../interfaces/ToDo.interface';

export default interface ToDoApi {
    retrieveAllTodosFromServer(): Promise<ToDo[]>;
    getToDoFromServer(toDoId: number): Promise<ToDo>;
    addToDoToServer(todo: ToDo): void;
    deleteToDoFromServer(todo: ToDo): void;
    updateToDoOnServer(todo: ToDo): void;
}
