/* eslint-disable class-methods-use-this */
import axios from 'axios';
import sleep from '../../utils/sleep';
import ToDo from '../../interfaces/ToDo.interface';
import ToDoApi from './ToDoApi.interface';

const baseUrl = 'http://localhost:3500'; // Implement this environment specific..

const todoPath = '/todos'; // Implement this environment specific..

// eslint-disable-next-line import/prefer-default-export
export default class ToDoApiJsonServer implements ToDoApi {
  public retrieveAllTodosFromServer = async (): Promise<ToDo[]> => {
    await sleep(2000);
    const { data } = await axios.get(baseUrl + todoPath);
    return data;
  };

  public getToDoFromServer = async (toDoId: number): Promise<ToDo> => axios.get(`${baseUrl + todoPath}/${toDoId}`).then((res) => res.data);

  public addToDoToServer = async (todo: ToDo) => {
    await axios.post(baseUrl + todoPath, todo);
  };

  public deleteToDoFromServer = async (todo: ToDo) => {
    await axios.delete(`${baseUrl + todoPath}/${todo.id}`);
  };

  public updateToDoOnServer = async (todo: ToDo) => {
    await axios.put(`${baseUrl + todoPath}/${todo.id}`, todo);
  };
}
