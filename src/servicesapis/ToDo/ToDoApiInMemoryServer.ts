/* eslint-disable class-methods-use-this */
/* eslint-disable react-hooks/rules-of-hooks */
import ToDo from '../../interfaces/ToDo.interface';
import ToDoApi from './ToDoApi.interface';
import useStateAsync from '../../hooks/useStateAsync';

export default class ToDoApiInMemoryServer implements ToDoApi {
  public todos: ToDo[];

  private setToDos: (toDos: ToDo[]) => void;

  private getToDos: () => Promise<ToDo[]>;

  constructor() {
    const [todos, setToDos, getToDos] = useStateAsync<ToDo[]>([
      {
        title: 'Wash clothes',
        description: 'Black wash, include dry option!',
        id: 1,
        complete: true,
      },
      {
        title: 'Clean bathrooms',
        description: "Don't forget the toilet!",
        id: 2,
        complete: true,
      },
    ]);
    this.todos = todos;
    this.setToDos = setToDos;
    this.getToDos = getToDos;
  }

  public retrieveAllTodosFromServer = async (): Promise<ToDo[]> => {
    const result = await this.getToDos();
    return this.sortArray(result);
  };

  public getToDoFromServer = async (toDoId: number): Promise<ToDo> => {
    const prevToDos = await this.getToDos();
    const result = prevToDos.filter((todoElement) => todoElement.id === toDoId)[0];
    return Promise.resolve(result);
  };

  public addToDoToServer = async (todo: ToDo) => {
    const prevToDos = await this.getToDos();
    const nextId = this.calculateMaxId(prevToDos);
    const newTodo = {
      ...todo,
      id: nextId,
    };
    await this.setToDos([
      ...prevToDos,
      newTodo,
    ]);
    await this.getToDos();
  };

  private calculateMaxId = (prevToDos: ToDo[]) => {
    const filteredIdArray = prevToDos.filter((todo) => todo.id !== undefined);
    const idArray = filteredIdArray.map((todo) => todo.id) as number [];
    return Math.max(...idArray) + 1;
  };

  private sortArray = (arrayWithObjects: ToDo[]) => arrayWithObjects.sort((a, b) => {
    if (a.id === undefined || b.id === undefined) return 1;
    if (a.id < b.id) return -1;
    if (a.id > b.id) return 1;
    return 0;
  });

  public deleteToDoFromServer(todo: ToDo): void {
    const oldToDos = this.todos;
    const newToDos = oldToDos.filter((todoelement) => todoelement.id !== todo.id);
    this.setToDos(newToDos);
  }

  public updateToDoOnServer = async (todo: ToDo) => {
    const prevToDos = await this.getToDos();
    const prevToDosMinusUpdatedTodo = prevToDos.filter((toDoInList) => toDoInList.id !== todo.id);
    const sortedOnId = this.sortArray(prevToDosMinusUpdatedTodo);
    this.setToDos([
      ...sortedOnId,
      todo,
    ]);
  };
}
