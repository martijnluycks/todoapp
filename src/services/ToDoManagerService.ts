/* eslint-disable react-hooks/rules-of-hooks */

import constate from 'constate';
import React, { SetStateAction } from 'react';
import useStateAsync from '../hooks/useStateAsync';
import ToDoApi from '../servicesapis/ToDo/ToDoApi.interface';
import ToDoApiJsonServer from '../servicesapis/ToDo/ToDoApiJsonServer';
import ToDoApiInMemoryServer from '../servicesapis/ToDo/ToDoApiInMemoryServer';
import ToDo from '../interfaces/ToDo.interface';
import { GlobalUIStateManagerService } from './GlobalUIStateManagerService';

// eslint-disable-next-line no-shadow
enum BackendApis {
  Memory = 'Memory',
  JSONServer = 'JSONServer',
}

class ToDoManagerService {
  public todos: ToDo[];

  public getToDos: () => Promise<ToDo[]>;

  private setToDos: React.Dispatch<SetStateAction<ToDo[]>>;

  private toDoApi: ToDoApi;

  constructor(
      private globalUIStateManagerService: GlobalUIStateManagerService,
  ) {
    const [todos, setToDos, getToDos] = useStateAsync<ToDo[]>([]);
    this.todos = todos;
    this.setToDos = setToDos;
    this.getToDos = getToDos;
    this.toDoApi = ToDoManagerService.getBackendApi(BackendApis.JSONServer); // Polymorphism */
  }

  private static getBackendApi = (backendApi: string): ToDoApi => {
    if (backendApi === BackendApis.JSONServer) {
      return new ToDoApiJsonServer();
    }
    if (backendApi === BackendApis.Memory) {
      return new ToDoApiInMemoryServer();
    }
    return new ToDoApiInMemoryServer();
  };

  emptyToDo = {
    id: undefined,
    title: '',
    description: '',
    complete: false,
  };

  public refreshToDos = async () => {
    // eslint-disable-next-line max-len
    this.globalUIStateManagerService.setIsBusy(true);
    const toDosfromServer = await this.toDoApi.retrieveAllTodosFromServer();
    this.setToDos([
      ...toDosfromServer,
      this.emptyToDo,
    ]);
    this.globalUIStateManagerService.setIsBusy(false);
  };

  // eslint-disable-next-line max-len
  public getToDoFromServer = async (todoId: number): Promise<ToDo> => this.toDoApi.getToDoFromServer(todoId);

  public addToDoToServer = async (todo: ToDo) => {
    await this.toDoApi.addToDoToServer(todo);
    await this.refreshToDos();
  };

  public deleteToDoFromServer = async (todo: ToDo) => {
    await this.toDoApi.deleteToDoFromServer(todo);
    await this.refreshToDos();
  };

  public updateToDoOnServer = async (todo: ToDo) => {
    await this.toDoApi.updateToDoOnServer(todo);
    await this.refreshToDos();
  };
}

// eslint-disable-next-line max-len
export const [ToDoManagerServiceProvider, useToDoManagerService] = constate(({ ui }:{ui: GlobalUIStateManagerService}) => new ToDoManagerService(ui));
