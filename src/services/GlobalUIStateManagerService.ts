/* eslint-disable react-hooks/rules-of-hooks */
import React, { SetStateAction } from 'react';
import constate from 'constate';
import useStateAsync from '../hooks/useStateAsync';

// eslint-disable-next-line no-shadow
export enum Application {
  ToDos = 'ToDos',
  Suggestions = 'Suggestions',
  Other = 'Other',
}

export class GlobalUIStateManagerService {
  public isBusy: boolean;

  public setIsBusy: React.Dispatch<SetStateAction<boolean>>;

  public getIsBusy: () => Promise<boolean>;

  public application: Application;

  public setApplication: React.Dispatch<SetStateAction<Application>>;

  public getApplication: () => Promise<Application>;

  constructor() {
    const [isBusy, setIsBusy, getIsBusy] = useStateAsync<boolean>(false);
    this.isBusy = isBusy;
    this.setIsBusy = setIsBusy;
    this.getIsBusy = getIsBusy;
    // eslint-disable-next-line max-len
    const [application, setApplication, getApplication] = useStateAsync<Application>(Application.ToDos);
    this.application = application;
    this.setApplication = setApplication;
    this.getApplication = getApplication;
  }
}

// eslint-disable-next-line max-len
export const [GlobalUIStateManagerServiceProvider, useGlobalUIStateManagerService] = constate(() => new GlobalUIStateManagerService());
