/* eslint-disable semi */
export default interface ToDo {
    title: string,
    description: string,
    id: number | undefined,
    complete: boolean,
}
