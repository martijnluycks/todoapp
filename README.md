HEADLINES:

##14-01-2022
# Converted from CRA (Create React App) to Vite

This project was initially created with [Create React App](https://github.com/facebook/create-react-app).
After a while we converted it to a Vite project.

## Available Scripts (updated after the conversion to Vite):

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The fake-backend will run simultaniously, and jest will concurrently run in watch mode.
The page will reload if you make edits.\
You will also see any linting errors in the console.

### `npm run build`

Builds the app for production to the `dist` folder.\
It correctly bundles React for production mode and optimizes the build for the best performance.
(minify, gzip, bundle)
The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

### `npm run serve`
After a successful run of 'npm run build' you can run this script to view a local
preview of the production build. Be aware this build needs a backend. 
If no backend is available you can run 'npm run fake-backend' for example, to preview
the full-stack app.

### `npm jest`

Launches the test runner in the interactive watch mode.

# Getting Started with 'fake backend' json-sever:
Using json-server as a fake backend for now.
This is a project dev dependency installed package, so you'll get this working out of the box automatically when running
'npm i' for this project.

Needed to manually add a json-server.json to configure making the homepage of json-server visible (done).

The fake backend database is kept IN this project. So if you run the next command, you'll be
able to start a fake backend by running (in a seperate command terminal):

npm run fake-backend

Which will run the fake backend json server at the port specified in the package.json file.
Side note:  in ToDoManagerService you'll find this line of code:
this.toDoApi = ToDoManagerService.getBackendApi(BackendApis.XXXX); // Polymorphism */

XXXX should be JSONServer to actually USE the JSONServer fake-backend. Look in the BackendApis enum to see the
options available ;-)